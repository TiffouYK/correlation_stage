# Scripts for analysis of RNA-BINDING protein vs jonction usage correlation.

## the arborescence of the input files is :
```
 ~/GTEX_CORR$ tree
.
├── GTEX_DATA
|   ├── GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_reads.gct
│   ├── GTEx_Analysis_2017-06-05_v8_STARv2.5.3a_junctions.gct
│   ├── GTEx_Analysis_v8_Annotations_SampleAttributesDS.txt
│   └── GTEx_Analysis_v8_Annotations_SubjectPhenotypesDS.txt
├── GTEX_OUT
└── PROCESSED_DATA
    ├── RNASEQ_JUNCTION_samples.txt
    ├── total_readspersample
    └── TP63_jct.gct

```
- GTEX_DATA contains original Data extracted from the GTEX portal
- PROCESSED_DATA contains files or results computed from the GTEX_DATA and required for the downstream analysis
- GTEX_OUT contains computed results

At this point there is 7 "modules"

- 1_INPUT_JCT_RBPs.R # to input junction count table for RNA Binding proteins to compute expression
- 1_INPUT_EXON_RBPs.R # to input gene expression data from GTEX
- 2_INPUT_JCT_GENEX.R # to input the junction count data for one/1 gene of interest (GOI)
- 3_ANNOTATION.R # to annotate input data with SUBJID, SMTS, SMTSD, SEX, AGE, DTHHRDY
- 4_FILTER_SAMPLE_TISSUE.R # to filter the tissues according to the computed expression of the GOI
- 5_CORREL_RBP_jctusage.R # to analyse correlation
- GRAPHIC.R # to produce plot of correlation
