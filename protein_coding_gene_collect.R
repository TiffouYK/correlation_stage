# Collect protein coding gene 

install.packages("xml2")

library(biomaRt)
library(xml2)

# define directories
# input directories by default
indir <- "~/GTEX_CORR"
raw_data_dir <- "GTEX_DATA"
processed_data_dir <-"PROCESSED_DATA"
# output directory
outdir <- "~/GTEX_CORR/ANNOTATION"
# define mart object to ensembl
ensembl <- useMart(biomart="ensembl", dataset="hsapiens_gene_ensembl")

# define input files
full_exon_data="GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_reads.gct"

# example_ids = c("ENSG00000172927", "ENSG00000224713", "ENSG00000135269", "ENSG00000272555", "ENSG00000013588")
# get the required info
res <- getBM(attributes=c("ensembl_gene_id","gene_biotype","external_gene_name","description"),filters = c("biotype"), values=list("protein_coding"), mart=ensembl)
dim(res)

# save the table of the genes with some annotation
write.table(file=file.path(outdir,"protein_coding_gene.csv"),res)

# load the table of the genes
data_genes <- read.table(file=file.path(outdir,"protein_coding_gene.csv"))

# collect annotation protein coding genes ENSG
infile=file.path(file.path(outdir,"protein_coding_gene.csv"))
outfile=file.path(file.path(indir,processed_data_dir), "ENSG_ID_protein_coding_gene_annotation.txt")
command=sprintf("tail -n+2 %s | awk '{print $2'} | sed 's/\"//g' > %s", infile, outfile)
system(command)

# collect all ENSG from the big GTEx file
infile=file.path(file.path(indir, raw_data_dir,full_exon_data))
outfile=file.path(file.path(indir,raw_data_dir), "ENSG_ID_protein_coding_gene.txt")
command=sprintf("tail -n+4 %s | awk '{print $1}' > %s", infile, outfile)
system(command)

# collect annotated ENSG
infile=file.path(file.path(indir,raw_data_dir), "ENSG_ID_protein_coding_gene.txt")
pattern=file.path(file.path(indir,processed_data_dir), "ENSG_ID_protein_coding_gene_annotation.txt")
outfile=file.path(file.path(indir,processed_data_dir), "ENSG_ID_protein_coding_gene_annotated.txt")
system(sprintf("grep -f %s %s > %s", pattern, infile, outfile))

